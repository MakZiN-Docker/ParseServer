#####
#Building and installing parse-server and bcrypt
#####

FROM node:lts-alpine AS builder

RUN apk --update --no-cache add python alpine-sdk

ARG PARSE_SERVER_VERSION
ADD https://github.com/parse-community/parse-server/archive/$PARSE_SERVER_VERSION.zip /

RUN cd / && \
    unzip $PARSE_SERVER_VERSION.zip && \
    rm /$PARSE_SERVER_VERSION.zip && \
    mv /parse-server-$PARSE_SERVER_VERSION /parse-server

RUN mkdir -p /parse-server/config

RUN mkdir -p /parse-server/cloud

WORKDIR /parse-server

RUN npm install bcrypt --build-from-source

RUN npm install && \
    npm run build

########################
FROM node:lts-alpine

COPY --from=builder /parse-server /parse-server

VOLUME /parse-server/config
VOLUME /parse-server/cloud

WORKDIR /parse-server

RUN npm install --save request && \
    npm install --save request-promise-native axios

ENV PORT=1337
EXPOSE $PORT

ENTRYPOINT ["npm", "start", "--"]