
Usage: parse-server [options] <path/to/configuration.json>


Options:

    --accountLockout [accountLockout]                                      account lockout policy for failed login attempts
    --allowClientClassCreation [allowClientClassCreation]                  Enable (or disable) client class creation, defaults to   true
    --allowCustomObjectId [allowCustomObjectId]                            Enable (or disable) custom objectId
    --allowHeaders [allowHeaders]                                          Add headers to Access-Control-Allow-Headers
    --allowOrigin [allowOrigin]                                            Sets the origin to Access-Control-Allow-Origin
    --analyticsAdapter [analyticsAdapter]                                  Adapter module for the analytics
    --appId <appId>                                                        Your Parse Application ID
    --appName [appName]                                                    Sets the app name
    --auth [auth]                                                          Configuration for your authentication providers, as  stringified JSON. See http://docs.parseplatform.org/parse-server/guide/#oauth-and-3rd-party-authentication
    --cacheAdapter [cacheAdapter]                                          Adapter module for the cache
    --cacheMaxSize [cacheMaxSize]                                          Sets the maximum size for the in memory cache, defaults  to 10000
    --cacheTTL [cacheTTL]                                                  Sets the TTL for the in memory cache (in ms), defaults to  5000 (5 seconds)
    --clientKey [clientKey]                                                Key for iOS, MacOS, tvOS clients
    --cloud [cloud]                                                        Full path to your cloud code main.js
    --cluster [cluster]                                                    Run with cluster, optionally set the number of processes   default to os.cpus().length
    --collectionPrefix [collectionPrefix]                                  A collection prefix for the classes
    --customPages [customPages]                                            custom pages for password validation and reset
    --databaseAdapter [databaseAdapter]                                    Adapter module for the database
    --databaseOptions [databaseOptions]                                    Options to pass to the mongodb client
    --databaseURI <databaseURI>                                            The full URI to your database. Supported databases are   mongodb or postgres.
    --directAccess [directAccess]                                          Replace HTTP Interface when using JS SDK in current node   runtime, defaults to false. Caution, this is an experimental feature that may not be appropriate for production.
    --dotNetKey [dotNetKey]                                                Key for Unity and .Net SDK
    --emailAdapter [emailAdapter]                                          Adapter module for email sending
    --emailVerifyTokenReuseIfValid [emailVerifyTokenReuseIfValid]          an existing email verify token should be reused when   resend verification email is requested
    --emailVerifyTokenValidityDuration [emailVerifyTokenValidityDuration]  Email verification token validity duration, in seconds
    --enableAnonymousUsers [enableAnonymousUsers]                          Enable (or disable) anonymous users, defaults to true
    --enableExpressErrorHandler [enableExpressErrorHandler]                Enables the default express error handler for all errors
    --enableSingleSchemaCache [enableSingleSchemaCache]                    Use a single schema cache shared across requests. Reduces  number of queries made to _SCHEMA, defaults to false, i.e. unique schema cache per request.
    --encryptionKey [encryptionKey]                                        Key for encrypting your files
    --expireInactiveSessions [expireInactiveSessions]                      Sets wether we should expire the inactive sessions,  defaults to true
    --fileKey [fileKey]                                                    Key for your files
    --filesAdapter [filesAdapter]                                          Adapter module for the files sub-system
    --graphQLPath [graphQLPath]                                            Mount path for the GraphQL endpoint, defaults to /graphql
    --graphQLSchema [graphQLSchema]                                        Full path to your GraphQL custom schema.graphql file
    --host [host]                                                          The host to serve ParseServer on, defaults to 0.0.0.0
    --idempotencyOptions [idempotencyOptions]                              Options for request idempotency to deduplicate identical   requests that may be caused by network issues. Caution, this is an experimental feature that may not be appropriate for   production.
    --javascriptKey [javascriptKey]                                        Key for the Javascript SDK
    --jsonLogs [jsonLogs]                                                  Log as structured JSON objects
    --liveQuery [liveQuery]                                                parse-server's LiveQuery configuration object
    --liveQueryServerOptions [liveQueryServerOptions]                      Live query server configuration options (will start the  liveQuery server)
    --loggerAdapter [loggerAdapter]                                        Adapter module for the logging sub-system
    --logLevel [logLevel]                                                  Sets the level for logs
    --logsFolder [logsFolder]                                              Folder for the logs (defaults to './logs'); set to null  to disable file based logging
    --masterKey <masterKey>                                                Your Parse Master Key
    --masterKeyIps [masterKeyIps]                                          Restrict masterKey to be used by only these ips, defaults  to [] (allow all ips)
    --maxLimit [maxLimit]                                                  Max value for limit option on queries, defaults to   unlimited
    --maxLogFiles [maxLogFiles]                                            Maximum number of logs to keep. If not set, no logs will   be removed. This can be a number of files or number of days. If using days, add 'd' as the suffix. (default: null)
    --maxUploadSize [maxUploadSize]                                        Max file size for uploads, defaults to 20mb
    --middleware [middleware]                                              middleware for express server, can be string or function
    --mountGraphQL [mountGraphQL]                                          Mounts the GraphQL endpoint
    --mountPath [mountPath]                                                Mount path for the server, defaults to /parse
    --mountPlayground [mountPlayground]                                    Mounts the GraphQL Playground - never use this option in   production
    --objectIdSize [objectIdSize]                                          Sets the number of characters in generated object id's,  default 10
    --passwordPolicy [passwordPolicy]                                      Password policy for enforcing password related rules
    --playgroundPath [playgroundPath]                                      Mount path for the GraphQL Playground, defaults to / playground
    --port [port]                                                          The port to run the ParseServer, defaults to 1337.
    --preserveFileName [preserveFileName]                                  Enable (or disable) the addition of a unique hash to the   file names
    --preventLoginWithUnverifiedEmail [preventLoginWithUnverifiedEmail]    Prevent user from login if email is not verified and   PARSE_SERVER_VERIFY_USER_EMAILS is true, defaults to false
    --protectedFields [protectedFields]                                    Protected fields that should be treated with extra   security when fetching details.
    --publicServerURL [publicServerURL]                                    Public URL to your parse server with http:// or https://.
    --push [push]                                                          Configuration for push, as stringified JSON. See http:// docs.parseplatform.org/parse-server/guide/#push-notifications
    --readOnlyMasterKey [readOnlyMasterKey]                                Read-only key, which has the same capabilities as  MasterKey without writes
    --restAPIKey [restAPIKey]                                              Key for REST calls
    --revokeSessionOnPasswordReset [revokeSessionOnPasswordReset]          When a user changes their password, either through the   reset password email or while logged in, all sessions are revoked if this is true. Set to false if you don't want to revoke   sessions.
    --scheduledPush [scheduledPush]                                        Configuration for push scheduling, defaults to false.
    --schemaCacheTTL [schemaCacheTTL]                                      The TTL for caching the schema for optimizing read/write   operations. You should put a long TTL when your DB is in production. default to 5000; set 0 to disable.
    --serverCloseComplete [serverCloseComplete]                            Callback when server has closed
    --serverStartComplete [serverStartComplete]                            Callback when server has started
    --serverURL <serverURL>                                                URL to your parse server with http:// or https://.
    --sessionLength [sessionLength]                                        Session duration, in seconds, defaults to 1 year
    --silent [silent]                                                      Disables console output
    --startLiveQueryServer [startLiveQueryServer]                          Starts the liveQuery server
    --userSensitiveFields [userSensitiveFields]                            Personally identifiable information fields in the user   table the should be removed for non-authorized users. Deprecated @see protectedFields
    --verbose [verbose]                                                    Set the logging to verbose
    --verifyUserEmails [verifyUserEmails]                                  Enable (or disable) user email validation, defaults to   false
    --webhookKey [webhookKey]                                              Key sent with outgoing webhook calls
    -h, --help                                                             display help for command
  Configure From Environment:

    $ PARSE_SERVER_ACCOUNT_LOCKOUT='accountLockout'
    $ PARSE_SERVER_ALLOW_CLIENT_CLASS_CREATION='allowClientClassCreation'
    $ PARSE_SERVER_ALLOW_CUSTOM_OBJECT_ID='allowCustomObjectId'
    $ PARSE_SERVER_ALLOW_HEADERS='allowHeaders'
    $ PARSE_SERVER_ALLOW_ORIGIN='allowOrigin'
    $ PARSE_SERVER_ANALYTICS_ADAPTER='analyticsAdapter'
    $ PARSE_SERVER_APPLICATION_ID='appId'
    $ PARSE_SERVER_APP_NAME='appName'
    $ PARSE_SERVER_AUTH_PROVIDERS='auth'
    $ PARSE_SERVER_CACHE_ADAPTER='cacheAdapter'
    $ PARSE_SERVER_CACHE_MAX_SIZE='cacheMaxSize'
    $ PARSE_SERVER_CACHE_TTL='cacheTTL'
    $ PARSE_SERVER_CLIENT_KEY='clientKey'
    $ PARSE_SERVER_CLOUD='cloud'
    $ PARSE_SERVER_CLUSTER='cluster'
    $ PARSE_SERVER_COLLECTION_PREFIX='collectionPrefix'
    $ PARSE_SERVER_CUSTOM_PAGES='customPages'
    $ PARSE_SERVER_DATABASE_ADAPTER='databaseAdapter'
    $ PARSE_SERVER_DATABASE_OPTIONS='databaseOptions'
    $ PARSE_SERVER_DATABASE_URI='databaseURI'
    $ PARSE_SERVER_ENABLE_EXPERIMENTAL_DIRECT_ACCESS='directAccess'
    $ PARSE_SERVER_DOT_NET_KEY='dotNetKey'
    $ PARSE_SERVER_EMAIL_ADAPTER='emailAdapter'
    $ PARSE_SERVER_EMAIL_VERIFY_TOKEN_REUSE_IF_VALID='emailVerifyTokenReuseIfValid'
    $ PARSE_SERVER_EMAIL_VERIFY_TOKEN_VALIDITY_DURATION='emailVerifyTokenValidityDuration'
    $ PARSE_SERVER_ENABLE_ANON_USERS='enableAnonymousUsers'
    $ PARSE_SERVER_ENABLE_EXPRESS_ERROR_HANDLER='enableExpressErrorHandler'
    $ PARSE_SERVER_ENABLE_SINGLE_SCHEMA_CACHE='enableSingleSchemaCache'
    $ PARSE_SERVER_ENCRYPTION_KEY='encryptionKey'
    $ PARSE_SERVER_EXPIRE_INACTIVE_SESSIONS='expireInactiveSessions'
    $ PARSE_SERVER_FILE_KEY='fileKey'
    $ PARSE_SERVER_FILES_ADAPTER='filesAdapter'
    $ PARSE_SERVER_GRAPHQL_PATH='graphQLPath'
    $ PARSE_SERVER_GRAPH_QLSCHEMA='graphQLSchema'
    $ PARSE_SERVER_HOST='host'
    $ PARSE_SERVER_EXPERIMENTAL_IDEMPOTENCY_OPTIONS='idempotencyOptions'
    $ PARSE_SERVER_JAVASCRIPT_KEY='javascriptKey'
    $ JSON_LOGS='jsonLogs'
    $ PARSE_SERVER_LIVE_QUERY='liveQuery'
    $ PARSE_SERVER_LIVE_QUERY_SERVER_OPTIONS='liveQueryServerOptions'
    $ PARSE_SERVER_LOGGER_ADAPTER='loggerAdapter'
    $ PARSE_SERVER_LOG_LEVEL='logLevel'
    $ PARSE_SERVER_LOGS_FOLDER='logsFolder'
    $ PARSE_SERVER_MASTER_KEY='masterKey'
    $ PARSE_SERVER_MASTER_KEY_IPS='masterKeyIps'
    $ PARSE_SERVER_MAX_LIMIT='maxLimit'
    $ PARSE_SERVER_MAX_LOG_FILES='maxLogFiles'
    $ PARSE_SERVER_MAX_UPLOAD_SIZE='maxUploadSize'
    $ PARSE_SERVER_MIDDLEWARE='middleware'
    $ PARSE_SERVER_MOUNT_GRAPHQL='mountGraphQL'
    $ PARSE_SERVER_MOUNT_PATH='mountPath'
    $ PARSE_SERVER_MOUNT_PLAYGROUND='mountPlayground'
    $ PARSE_SERVER_OBJECT_ID_SIZE='objectIdSize'
    $ PARSE_SERVER_PASSWORD_POLICY='passwordPolicy'
    $ PARSE_SERVER_PLAYGROUND_PATH='playgroundPath'
    $ PORT='port'
    $ PARSE_SERVER_PRESERVE_FILE_NAME='preserveFileName'
    $ PARSE_SERVER_PREVENT_LOGIN_WITH_UNVERIFIED_EMAIL='preventLoginWithUnverifiedEmail'
    $ PARSE_SERVER_PROTECTED_FIELDS='protectedFields'
    $ PARSE_PUBLIC_SERVER_URL='publicServerURL'
    $ PARSE_SERVER_PUSH='push'
    $ PARSE_SERVER_READ_ONLY_MASTER_KEY='readOnlyMasterKey'
    $ PARSE_SERVER_REST_API_KEY='restAPIKey'
    $ PARSE_SERVER_REVOKE_SESSION_ON_PASSWORD_RESET='revokeSessionOnPasswordReset'
    $ PARSE_SERVER_SCHEDULED_PUSH='scheduledPush'
    $ PARSE_SERVER_SCHEMA_CACHE_TTL='schemaCacheTTL'
    $ PARSE_SERVER_SERVER_CLOSE_COMPLETE='serverCloseComplete'
    $ PARSE_SERVER_SERVER_START_COMPLETE='serverStartComplete'
    $ PARSE_SERVER_URL='serverURL'
    $ PARSE_SERVER_SESSION_LENGTH='sessionLength'
    $ SILENT='silent'
    $ PARSE_SERVER_START_LIVE_QUERY_SERVER='startLiveQueryServer'
    $ PARSE_SERVER_USER_SENSITIVE_FIELDS='userSensitiveFields'
    $ VERBOSE='verbose'
    $ PARSE_SERVER_VERIFY_USER_EMAILS='verifyUserEmails'
    $ PARSE_SERVER_WEBHOOK_KEY='webhookKey'

  Get Started guide:

    Please have a look at the get started guide!
    http://docs.parseplatform.org/parse-server/guide/


  Usage with npm start

    $ npm start -- path/to/config.json
    $ npm start -- --appId APP_ID --masterKey MASTER_KEY --serverURL serverURL
    $ npm start -- --appId APP_ID --masterKey MASTER_KEY --serverURL serverURL


  Usage:

    $ parse-server path/to/config.json
    $ parse-server -- --appId APP_ID --masterKey MASTER_KEY --serverURL serverURL
    $ parse-server -- --appId APP_ID --masterKey MASTER_KEY --serverURL serverURL